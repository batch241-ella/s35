const express = require("express");
// Mongoose is a package that allows creation of Schemas to model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 3000;

// [Section] MongoDB connection

// Connect to the database by passing in your connection string, remember to replace the password and database names with actual values
// Syntax
	// mongoose.connect("<MongoDB connection string>", { useNewUrlParser : true });

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.fokbgo5.mongodb.net/s35?retryWrites=true&w=majority", 
	{ 
		// Due to updates in Mongo DB drivers that allow connection to it, the default connection string is being flagged as an error
		// By default a warning will be displayed in the terminal when the application is run, but this will not prevent Mongoose from being used in the application
		// { newUrlParser : true } allows us to avoid any current and future errors while connecting to Mongo DB
		useNewUrlParser : true,  
		useUnifiedTopology : true
	}
);

// Connecting to MongoDB locally
// Set notifications for connection success or failure
// Works with the on and once Mongoose methods
let db = mongoose.connection; 

// If a connection error occurred, output in the console
// "console.error.bind(console)" allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// [Section] Mongoose Schemas

// Schemas determine the structure of the documents to be written in the database
// Schemas act as blueprints to our data
// The "new" keyword creates a new Schema
const taskSchema = new mongoose.Schema({ 
	// Define the fields with the corresponding data type
	name : String,
	// There is a field called "status" that is a "String" and the default value is "pending"
	status : { 
		type : String,
		// Default values are the predefined values for a field if we don't put any value
		default : "pending"
	};
});

// [Section] Models

// Uses schemas and are used to create/instantiate objects that correspond to the schema
// Models must be in singular form and capitalized
// The first parameter of the Mongoose model method indicates the collection in where to store the data
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
const Task = mongoose.model("Task", taskSchema); 

// [Section] Creation of todo list routes
// Allows your app to read json data
app.use(express.json());
// Allows your app to read other data types
app.use(express.urlencoded({extended:true})); 


// Creating a new task
/*
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return an error
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req, res)=> {

	// Check if there are duplicate tasks
	// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
	Task.findOne({name : req.body.name}, (err, result) => {

		// If a document was found and the document's name matches the information sent via the client/Postman
		if(result != null && result.name == req.body.name){

			// Return a message to the client/Postman
			return res.send("Duplicate task found");

		// If no document was found
		} else {

			// Create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			});

			// The "save" method will store the information to the database
			newTask.save((saveErr, savedTask) => {

				// If there are errors in saving
				if(saveErr){

					// Will print any errors found in the console
					return console.error(saveErr);

				// No error found while creating the document
				} else {

					// Return a status code of 201 for created
					// Sends a message "New task created" on successful creation
					return res.status(201).send("New task created");

				};
			});
		};

	});
});


// Getting all the tasks
// Business Logic
/*
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/
app.get("/tasks", (req, res) => {

	// "find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
	Task.find({}, (err, result) => {

		// If an error occurred
		if (err) {

			// Will print any errors found in the console
			return console.log(err);

		// If no errors are found
		} else {

			// Status "200" means that everything is "OK" in terms of processing
			// The "json" method allows to send a JSON format for the response
			// The returned response is purposefully returned as an object with the "data" property to mirror real world complex data structures
			return res.status(200).json({
				data : result			
			});
		};
	});
});

app.listen(port, () => console.log(`Server running at port ${port}`));




