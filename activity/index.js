const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3000;


mongoose.connect("mongodb+srv://admin123:admin123@cluster0.fokbgo5.mongodb.net/s35?retryWrites=true&w=majority", 
	{ 
		useNewUrlParser : true,  
		useUnifiedTopology : true
	}
);

let db = mongoose.connection; 

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

const userSchema = new mongoose.Schema({ 
	username: String,
	password: String
});

const User = mongoose.model("User", userSchema); 

app.use(express.json());
app.use(express.urlencoded({extended:true})); 


app.post("/signup", (req, res)=> {

	User.findOne({username: req.body.username, password: req.body.password}, (err, result) => {
		if (err) {
			return console.log("An error has occured");
		} else if (result != null && req.body.username == result.username) {
			return res.send("Duplicate user found");
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
			// push to db
			newUser.save((saveErr, savedUser) => {
				if (saveErr) {
					return console.log(saveErr);
				} 
				return res.status(201).send("New user registered");
			});
		}
	})
});


app.get("/users", (req, res) => {
	User.find({}, (err, result) => {
		if (err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				users : result			
			});
		};
	});
});

app.listen(port, () => console.log(`Server running at port ${port}`));




